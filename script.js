
function preLoad(dist){
    let loader = document.createElement('div')
    loader.classList.add('lds-dual-ring')
    loader.setAttribute('bis_skin_checked', 1)
    dist.append(loader)
}

let i = 0
url = 'https://ajax.test-danit.com/api/swapi/films'
const ul = document.createElement('ul')
fetch(url)
    .then(response=>response.json())
    .then((data)=>{
        for(film of data){
            let li = document.createElement('li')
            li.id = i
            li.innerHTML = `
                Episode: ${film.episodeId}<br>
                Title: ${film.name}<br>
                Opening Crawl: ${film.openingCrawl}<br>
            `;
            ul.append(li)
            preLoad(li)
            document.body.append(ul)
            i++
        }
        i = 0
        return data
    })
    .then(data => {
        console.log(data)
        for(film of data){
            document.querySelector('.lds-dual-ring').remove()
            let li = document.getElementById(i)
            li.innerText += 'Characters: '
            film.characters.forEach(element => {
                fetch(element).then(response=>response.json()).then(person=>{
                    li.innerHTML += person.name + ', '
                })
            })
            i++
        }
    })

